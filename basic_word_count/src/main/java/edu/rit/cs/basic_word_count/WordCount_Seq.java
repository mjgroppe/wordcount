package edu.rit.cs.basic_word_count;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.rit.cs.basic_word_count.CommonMethods.*;

//This class is used for the sequential word counting process
public class WordCount_Seq {
    public static void main(String[] args) {
        MyTimer myTimer = new MyTimer("wordCount");
        myTimer.start_timer();
        List<String> words = CommonMethods.tokens();
        System.out.println(words.size());
        Map<String, Integer> wordCount = new HashMap<>();
        for(String word : words) {
            if(!wordCount.containsKey(word)) {
                wordCount.put(word, 1);
            } else{
                int init_value = wordCount.get(word);
                wordCount.replace(word, init_value, init_value+1);
            }
        }
        AlphabetSort s = new AlphabetSort(wordCount);
        s.quicksort(0, wordCount.size()-1);
        List<String> wods = s.words;
        print_sorted_word_count(wods, wordCount);
        myTimer.stop_timer();
        myTimer.print_elapsed_time();
    }

}
