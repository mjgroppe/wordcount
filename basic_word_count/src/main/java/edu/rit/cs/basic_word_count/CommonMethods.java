package edu.rit.cs.basic_word_count;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import edu.rit.cs.basic_word_count.*;
//This class is used to store any methods shared within the other classes
public class CommonMethods {
    //Note: because I was having issues using a relative path, I simply made this repository flat.
    public static final String AMAZON_FINE_FOOD_REVIEWS_file="Reviews.csv";

    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }
    public static void print_sorted_word_count( List<String> words, Map<String, Integer> wordcount){
        for(String word : words){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }
    public static void print_word_count( Map<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }
    private static String alphabetize(String wordA, String wordB, int marker) {
        if(wordA.length() < marker+1) {
            return wordB;
        } else if(wordB.length() < marker+1) {
            return wordA;
        } else if(wordA.charAt(marker) > wordB.charAt(marker)){
            return wordA;
        } else if(wordA.charAt(marker) < wordB.charAt(marker)){
            return wordB;
        } else if(wordA.equals(wordB)) {
            return wordA;
        } else if(wordA.charAt(marker) == wordB.charAt(marker)) {
            return alphabetize(wordA, wordB, marker+1);
        } else {
            return wordA;
        }
    }
    private static int partition(List<String> words, int start, int end) {
        String pivot = words.get(end);
        int j  = start-1;
        for(int i = start; i < end; i++) {
            String upper = alphabetize(words.get(i), pivot, 0);
            if(upper.equals(pivot)) {
                j++;
                Collections.swap(words, j, i);
            }
        }
        j++;
        Collections.swap(words, j, end);
        return j;
    }
    public static ArrayList<String> tokens() {
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);

        /* Tokenize words */
        ArrayList<String> words = new ArrayList<String>();
        for (AmazonFineFoodReview review : allReviews) {
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            Matcher matcher = pattern.matcher(review.get_Summary());

            while (matcher.find())
                words.add(matcher.group().toLowerCase());
        }
        return words;
    }
    public static void quicksort(List<String> words,  int start, int end) {
        if(start < end) {
            int p = partition( words, start, end);
            quicksort(words, start, p - 1);
            quicksort(words, p+1, end);
        }
    }
    public static HashMap<String, Integer> merge(ArrayList<HashMap<String, Integer>> sep) {
        HashMap<String, Integer> total = new HashMap<>();
        for(int i = 0; i < sep.size(); i++) {
            Set<String> keys = sep.get(i).keySet();
            Object[] ks = keys.toArray();
            for (int j = 0; j < keys.size(); j++) {
                String key = (String)ks[j];
                if (!total.containsKey(key)) {
                    total.put(key, sep.get(i).get(key));
                } else {
                    int value1 = total.get(key);
                    int value2 = sep.get(i).get(key);
                    total.replace(key, value1, value1 + value2);
                }
            }
        }
        return total;
    }
}
