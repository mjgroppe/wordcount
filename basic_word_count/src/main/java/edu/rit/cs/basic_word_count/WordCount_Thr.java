package edu.rit.cs.basic_word_count;

import java.util.*;

import static edu.rit.cs.basic_word_count.CommonMethods.*;
//This class is for processing and counting word instances using a threading method

class ThreadMethods extends Thread {
    List<String> Uncounted;
    HashMap<String, Integer> Counted;
    public ThreadMethods(List<String> Uncounted) {
        this.Uncounted = Uncounted;
        this.Counted = new HashMap<>();
    }
    public void run() {
        for(String word : Uncounted) {
            if(!Counted.containsKey(word)) {
                Counted.put(word, 1);
            } else{
                int init_value = Counted.get(word);
                Counted.replace(word, init_value, init_value+1);
            }
        }
    }
    public HashMap<String, Integer> getResult() {
        return Counted;
    }
}
public class WordCount_Thr {
        public static int Thr_Size = 10000;
        //Because this method is used by this class and by the classes in part three, this method does all
        //the heavy lifting for this class
        public static HashMap<String, Integer> threadedCount(List<String> words) {
            /* Count words */
            MyTimer myTimer = new MyTimer("wordCount");
            myTimer.start_timer();
            int threadcount = (words.size()/Thr_Size);
            int x = 0;
            ArrayList<ThreadMethods> threads = new ArrayList<>();
            for(int i = 0; i < threadcount; i++) {
                int end = 0;
                if((x+Thr_Size) > words.size()-1) {
                    end = words.size()-1;
                } else {
                    end = x+Thr_Size;
                }
                List<String> s = new ArrayList<>();
                s.addAll(words.subList(x, end));
                x = x + Thr_Size + 1;
                ThreadMethods t = new ThreadMethods(s);
                threads.add(t);
                t.start();
                if(x>words.size()-1) {
                    break;
                }
            }
            ArrayList<HashMap<String, Integer>> res = new ArrayList<>();
            for(ThreadMethods t : threads) {
                try {
                    t.join();
                    res.add(t.getResult());
                } catch(java.lang.InterruptedException e){
                    break;
                }
            }
            myTimer.stop_timer();
            HashMap<String, Integer> wordCount = merge(res);
            AlphabetSort s = new AlphabetSort(wordCount);
            s.quicksort(0, wordCount.size()-1);
            return wordCount;
        }
        public static void main(String[] args) {
            MyTimer time = new MyTimer("Total Time Elapsed");
            time.start_timer();
            List<String> words = tokens();
            HashMap<String, Integer> wordCount = threadedCount(words);
            AlphabetSort s = new AlphabetSort(wordCount);
            s.quicksort(0, wordCount.size()-1);
            ArrayList<String> wordList = s.words;
            print_sorted_word_count(wordList, wordCount);
            time.stop_timer();
            time.print_elapsed_time();

       }
}
