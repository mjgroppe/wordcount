package edu.rit.cs.basic_word_count;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

public class WordCount_Con_Worker {

    public static void main(String[] args) {
        int port = Config.port;
        List<String> word = new ArrayList<>();
        try {
            ServerSocket server = new ServerSocket(port);
            Socket parent = server.accept();
            System.out.println("Connection achieved...");
            DataInputStream fromParent = new DataInputStream(new BufferedInputStream(parent.getInputStream()));
            String line = "";
            System.out.println("Reading in...");
            while(true) {
                try {
                    line = fromParent.readUTF();
                    word.add(line);
                    if(line.equals("")) {
                        break;
                    }
                } catch(IOException e) {
                    System.out.println(e.getMessage());
                    break;
                }
            }
            System.out.println("Reading complete...");
            fromParent.close();
            parent.close();
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            Socket parent = new Socket(Config.MasterIP, Config.port);
            DataOutputStream toParent = new DataOutputStream(parent.getOutputStream());
            System.out.println("Connection made...");
            HashMap<String, Integer> res = WordCount_Thr.threadedCount(word);
            Set<String> t = res.keySet();
            System.out.println("Counting complete. Sending back...");
            for(String s : t) {
                try {
                    toParent.writeUTF(s + ","+res.get(s).toString());
                } catch(IOException e) {
                    System.out.println(e.getMessage());
                    System.out.println("Writing failed in foreach");
                    break;
                }
            }
            System.out.println("Writing complete...");
            parent.close();
            toParent.close();
        } catch(IOException e) {
            System.out.println(e.getMessage());
            System.out.println("Writing failed");
        }
        System.out.println("Execution complete. Closing...");
    }
}
