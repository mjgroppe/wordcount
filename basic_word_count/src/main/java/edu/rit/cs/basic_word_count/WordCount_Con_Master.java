package edu.rit.cs.basic_word_count;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static edu.rit.cs.basic_word_count.CommonMethods.merge;
import static edu.rit.cs.basic_word_count.CommonMethods.print_sorted_word_count;
//This class is used to allow concurrency within the sending process of data to nodes
class Info extends Thread {
    int i;
    List<String> sub;
    Info(int i, List<String> sub) {
        this.i = i;
        this.sub = sub;
    }
    public void run() {
        try {
            Socket worker = new Socket(Config.WorkerIPs[this.i], Config.port);
            DataOutputStream toWorker = new DataOutputStream(worker.getOutputStream());
            for(int i = 0; i < sub.size(); i++) {
                toWorker.writeUTF(sub.get(i));
            }
            worker.close();
            toWorker.close();
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
//This class is used for the Master node in the 5 node system for part three
//The only command line argument used in this class is the number of worker nodes
public class WordCount_Con_Master {

    public static void main(String[] args) {
        MyTimer time = new MyTimer("Total Time Elapsed");
        time.start_timer();
        System.out.println("Parsing Document...");
        List<String> words = CommonMethods.tokens();
        int machineCount = Integer.parseInt(args[0]);
        int approxListSize = (int)Math.ceil(words.size()/machineCount);
        int diff = words.size()%machineCount;
        for(int i = 0; i < diff; i++) {
            words.add("");
        }
        System.out.println("Handing out smaller lists and starting concurrency...");
        ArrayList<Info> infos = new ArrayList<>();
        ArrayList<HashMap<String, Integer>> maps = new ArrayList<>();
        for(int i = 0; i < machineCount; i++) {
            List<String> word = words.subList(i*approxListSize, (i+1)*approxListSize);
            Info is = new Info(i, word);
            infos.add(is);
            is.start();
        }
        System.out.println("Threads done.");
        for(Info i : infos) {
            try {
                i.join();
                ServerSocket server = new ServerSocket(Config.port);
                Socket worker = server.accept();
                DataInputStream fromWorker = new DataInputStream(new BufferedInputStream(worker.getInputStream()));
                String line = "";
                HashMap<String, Integer> map  = new HashMap<>();
                while(true) {
                    try {
                        line = fromWorker.readUTF();
                        if (line.equals("")) {
                            break;
                        } else {
                            String[] pair = line.split(",", 2);
                            int value = Integer.parseInt(pair[1]);
                            map.put(pair[0], value);
                        }
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                        break;
                    }
                }
                fromWorker.close();
                worker.close();
                maps.add(map);
            } catch(IOException | InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println("Counting complete. Merging now.");
        HashMap<String, Integer> wordcount = merge(maps);
        AlphabetSort s = new AlphabetSort(wordcount);
        s.quicksort(0, wordcount.size()-1);
        ArrayList<String> wods = s.words;
        print_sorted_word_count(wods, wordcount);
        time.stop_timer();
        time.print_elapsed_time();
    }

}
