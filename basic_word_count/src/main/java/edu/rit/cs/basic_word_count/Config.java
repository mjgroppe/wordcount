package edu.rit.cs.basic_word_count;

public class Config {
    //this class is used to store the port and IP addresses of the machines to be used
    public static int port = 8080;
    public static String[] WorkerIPs = {"172.17.0.3", "172.17.0.4", "172.17.0.5", "172.17.0.6"};
    public static String MasterIP = "172.17.0.2";
}
