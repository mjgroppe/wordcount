package edu.rit.cs.basic_word_count;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
//This class uses quicksort to alphabetize the list of words given
public class AlphabetSort {
    public Map<String, Integer> UnSorted;
    //public ArrayList<Pair<String, Integer>> Sorted;
    public ArrayList<String> words;
    AlphabetSort(Map<String, Integer> UnSorted) {
        this.UnSorted = UnSorted;
        Set<String> s = UnSorted.keySet();
        this.words = new ArrayList<String>(s);
    }
    private String alphabetize(String wordA, String wordB, int marker) {
        if(wordA.length() < marker+1) {
            return wordB;
        } else if(wordB.length() < marker+1) {
            return wordA;
        } else if(wordA.charAt(marker) > wordB.charAt(marker)){
            return wordA;
        } else if(wordA.charAt(marker) < wordB.charAt(marker)){
            return wordB;
        } else if(wordA.equals(wordB)) {
            return wordA;
        } else if(wordA.charAt(marker) == wordB.charAt(marker)) {
            return alphabetize(wordA, wordB, marker+1);
        } else {
            return wordA;
        }
    }
    private int partition(int start, int end) {
        String pivot = words.get(end);
        int j  = start-1;
        for(int i = start; i < end; i++) {
            String upper = alphabetize(words.get(i), pivot, 0);
            if(upper.equals(pivot)) {
                j++;
                Collections.swap(words, j, i);
            }
        }
        j++;
        Collections.swap(words, j, end);
        return j;
    }
    public void quicksort(int start, int end) {
        if(start < end) {
            int p = partition( start, end);
            quicksort(start, p - 1);
            quicksort(p+1, end);
        }
    }
}
