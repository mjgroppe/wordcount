### Download the dataset
* Download the ["Amazon fine food reviews"](https://www.kaggle.com/snap/amazon-fine-food-reviews/downloads/amazon-fine-food-reviews.zip/2) dataset
* Extract a file "Reviews.csv" into the folder with the .java files
```
amazon-fine-food-reviews/Reviews.csv
``` 

### Build this example as a jar
```
mvn package
```

### Run this example in this folder
```
java -cp target/basic_word_count-1.0-SNAPSHOT.jar edu.rit.cs.basic_word_count.WordCount_Seq
```
### Running instructions for Part 3
* Create 5 docker containers using the image peiworld/csci652
* Move the entire basic_word_count folder into each container at the root folder
* Ensure that the IP addresses and port number in the config file are correct
* Ensure that the Reviews.csv file is in the same folder with all the .java files
* Run each of the worker nodes with this command:
```
java -cp "target/classes" edu.rit.cs.basic_word_count.WordCount_Con_Worker
```
* Run the master node with this command:
```
java -cp "target/classes" edu.rit.cs.basic_word_count.WordCount_Con_Master 4
```
* Each node will print statements to keep you updated on their progress, and the master node will print the sorted counted list once its all finished.
### Issues
- “Exception in thread “main” java.lang.OutOfMemoryError: Java heap space” error. One naive solution is increase the memory. But, you need to think about how to optimize the program, so that you can avoid this naive fix.
```
export JVM_ARGS="-Xmx1024m -XX:MaxPermSize=256m"
```