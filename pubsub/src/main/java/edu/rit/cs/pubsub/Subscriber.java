package edu.rit.cs.pubsub;

public interface Subscriber {
	/*
	 * subscribe to a topic
	 */
    void subscribe(Topic topic);
	
	/*
	 * subscribe to a topic with matching keywords
	 */
    void subscribe(String keyword);
	
	/*
	 * unsubscribe from a topic 
	 */
    void unsubscribe(Topic topic);
	
	/*
	 * unsubscribe to all subscribed topics
	 */
    void unsubscribe();
	
	/*
	 * show the list of topics current subscribed to 
	 */
    void listSubscribedTopics();
	
}
