package edu.rit.cs.pubsub;

public interface Publisher {
	/*
	 * publish an event of a specific topic with title and content
	 */
    void publish(Event event);
	
	/*
	 * advertise new topic
	 */
    void advertise(Topic newTopic);
}
